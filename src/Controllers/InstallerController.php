<?php

namespace Lixir\Installer\Controllers;

use Exception;
use Illuminate\Foundation\Console\Artisan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\ClientRepository;
use Lixir\Ecommerce\Models\Database\Configuration;
use Lixir\Ecommerce\Models\Database\Role;
use Lixir\Theme\Facade as Theme;
use Lixir\Installer\Requests\AdminUserRequest;

class InstallerController extends Controller
{
    protected $service;

    protected $clientRepository;

    public $extensions = [
        'openssl',
        'pdo',
        'mbstring',
        'tokenizer',
        'gd',
        'xml',
        'ctype',
        'json',
        'curl'
    ];

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function index()
    {
        Session::forgot('install-module');

        $result = [];

        foreach ($this->extensions as $ext) {
            if (extension_loaded($ext)) {
                $result[$ext] = 'true';
            } else {
                $result[$ext] = false;
            }
        }

        return view('lixir-install::install.extension')->with('result', $result);
    }

    public function databaseEnvGet()
    {
        return view('lixir-install::install.database-env');
    }

    public function databaseEnvPost(Request $request)
    {
        $envReplaceKeys = ['db_database', 'db_username', 'db_password'];
        $envFilePath = App::environmentFilePath();

        foreach ($envReplaceKeys as $formKey) {
            $envKey = strtoupper($formKey);
            $envCurrentValue = env($envKey);
            $dbFormValue = $request->get($formKey);

            file_put_contents($envFilePath, preg_replace("/^{$envKey}={$envCurrentValue}/m"), "{$envKey}=".$dbFormValue, file_get_contents($envFilePath));
        }

        return view('lixir-install::install-database-table');
    }

    public function databaseTableGet()
    {
        return view('lixir-install::install-database-table');
    }

    public function databaseTablePost(Request $request)
    {
        $downloadFile = false;

        try {
            if (1 == $request->get('selected_db_option')) {
                Artisan::call('migrate:fresh');
            }

            if (2 == $request->get('selected_db_option')) {
                $downloadFile = true;
                $pathToFile = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "sql" . DIRECTORY_SEPARATOR . "lixir-ecommerce-table.sql";
            }

            if (3 == $request->get('selected_db_option')) {
                $downloadFile = true;
                $pathToFile = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "sql" . DIRECTORY_SEPARATOR . "lixir-ecommerce-sample-data.sql";
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        if (true === $downloadFile) {
            return response()->download($pathToFile);
        } else {
            return redirect()->route('lixir.install.database.data.get');
        }
    }

    public function databaseDataGet()
    {
        return view('lixir-install::install.database-data');
    }

    public function databaseDataPost(Request $request)
    {
        if ($request->get('install_data') == "yes") {
            $fromPath = __DIR__ . "/../../assets";
            $toPath = public_path();

            try {
                Artisan::call('db:seed', ['--class' => 'LixirDataSeeder']);
                Theme::publishItem($fromPath, $toPath);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
        }

        return redirect()->route('lixir.install.admin');
    }

    public function admin()
    {
        return view('lixir-install::install.admin');
    }

    public function adminPost(AdminUserRequest $request)
    {
        $theme = Theme::get('lixir-default');
        $fromPath = $theme['asset_path'];
        $toPath = public_path('vendor/' . $theme['identifier']);

        Theme::publishItem($fromPath, $toPath);

        try {
            $role = Role::create([
                'name' => 'administrator',
                'description' => 'Administrator Role has all access'
            ]);

            $adminUser = AdminUser::create([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'is_super_admin' => 1,
                'role_id' => $role->id
            ]);

            Artisan::call('passport:install');

            $this->clientRepository->createPasswordGrantClient($adminUser->id, $adminUser->full_name, $request->getUriForPath('/'));

            Configuration::create([
                'configuration_key' => 'active_theme_identifier',
                'configuration_value' => 'lixir-default'
            ]);
            Configuration::create([
                'configuration_key' => 'active_theme_path',
                'configuration_value' => base_path('themes\lixir\default')
            ]);
            Configuration::create([
                'configuration_key' => 'lixir_catalog_no_of_product_category_page',
                'configuration_value' => 9
            ]);
            Configuration::create([
                'configuration_key' => 'lixir_catalog_cart_page_display_taxamount',
                'configuration_value' => 'yes'
            ]);
            Configuration::create([
                'configuration_key' => 'lixir_tax_class_percentage_of_tax',
                'configuration_value' => 15
            ]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return redirect()->route('lixir.install.success');
    }

    public function success()
    {
        Storage::disk('local')->put('installed.txt', '.installed');

        return view('lixir-install::install.success');
    }
}
