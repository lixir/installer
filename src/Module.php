<?php

namespace Lixir\Installer;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Lixir\Installer\Middleware\InstallerMiddleware;

class Module extends ServiceProvider
{
    public function boot()
    {
        $this->registerMiddleware();
        $this->registerViewPath();
        $dbFactory = $this->app[EloquentFactory::class];
        $path = __DIR__ . "/../database/factories";
        $dbFactory->load($path);
    }

    public function register()
    {
        $this->mapWebRoutes();
    }

    public function registerMiddleware()
    {
        $router = $this->app['router'];
        $router->aliasMiddleware('installer', InstallerMiddleware::class);
    }

    protected function mapWebRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
    }

    protected function registerViewPath()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'lixir-install');
    }
}
