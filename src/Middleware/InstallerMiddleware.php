<?php

namespace Lixir\Installer\Middleware;

use Closure;
use Illuminate\Support\Facades\Storage;

class InstallerMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!Storage::disk('local')->has('installed.txt')) {
            return redirect()->to('/install');
        }

        $installFile = Storage::disk('local')->has('installed.txt');
        if ('.installed' != $installFile) {
            return redirect()->to('/install');
        }

        return $next($request);
    }
}
