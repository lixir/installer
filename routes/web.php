<?php

Route::middleware('web')->namespace('Lixir\Installer\Controllers')->prefix('install')->group(function () {
    Route::get('', 'InstallerController@index')->name('install.index');
    Route::get('db','InstallerController@databaseEnvGet')->name('lixir.db');
    Route::post('db','InstallerController@databaseEnvPost')->name('lixir.db.post');
    Route::get('db/table','InstallerController@databaseTableGet')->name('lixir.install.database.table.get');
    Route::post('db/table','InstallerController@databaseTablePost')->name('lixir.install.database.table.post');
    Route::get('db/data','InstallerController@databaseDataGet')->name('lixir.install.database.data.get');
    Route::post('db/data','InstallerController@databaseDataPost')->name('lixir.install.database.data.post');
    Route::get('admin','InstallerController@admin')->name('lixir.install.admin');
    Route::post('admin','InstallerController@adminPost')->name('lixir.install.admin.post');
    Route::get('success','InstallerController@success')->name('lixir.install.success');
});

