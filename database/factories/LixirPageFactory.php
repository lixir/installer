<?php

use Faker\Generator as Faker;

$factory = define(\Lixir\Ecommerce\Models\Database\Page::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = str_slug($name);
    $content = $faker->realText(5000);
    $metaTitle = $faker->title;
    $metaDescription = $faker->title;

    return [
        'name' => $name,
        'slug' => $slug,
        'content' => $content,
        'meta_title' => $metaTitle,
        'meta_description' => $metaDescription
    ];
});
