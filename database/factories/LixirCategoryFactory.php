<?php

use Faker\Generator as Faker;

$factory = define(\Lixir\Ecommerce\Models\Database\Category::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = str_slug($name);

    return [
        'parent_id' => 0,
        'name' => $name,
        'slug' => $slug
    ];
});
